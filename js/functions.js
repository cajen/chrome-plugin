String.prototype.format = function () {
    let args = [].slice.call(arguments);
    return this.replace(/(\{\d+\})/g, function (a){
        return args[+(a.substr(1,a.length-2))||0];
    });
};

/**
 * Quick wrapper for creating a DOM object out of an URL
 * @param url
 * @returns {jQuery|HTMLElement}
 */
let parseUrl = function(url)
{
    return $('<a>', {href: url});
};

/**
 * A function to replace all plusses (substitute for spaces) in a GET parameter
 * @param parameter_value
 * @returns {string}
 */
let normalizeGetParameter = function(parameter_value)
{
    let plus_replaced = parameter_value.replace(/\+/g, ' ');
    return decodeURIComponent(plus_replaced);
};

/**
 *
 * @param url
 * @param parameter
 * @returns {boolean|string}
 */
let getUrlParameter = function(url, parameter) {

    // Get the query part of the given URL and remove the question mark
    let query_string = parseUrl(url).prop('search').replace('?', '');

    // Split the query on & to get the key=value pairs separated by an equal sign
    let get_parameters = query_string.split('&'),
        parameter_name,
        i;

    for (i = 0; i < get_parameters.length; i++) {
        // Split the key=value pairs so we can compare the key
        parameter_name = get_parameters[i].split('=');

        // Check if the acquired key is equal to the asked key
        if (parameter_name[0] === parameter) {

            // If the parameter value doesn't exist it means that it is defined
            // which evaluates to true. Otherwise we normalize the parameter and return it
            return parameter_name[1] === undefined ? true : normalizeGetParameter(parameter_name[1]);
        }
    }
};

/**
 * Check if the given hostname is supported, if so return the lowercase
 * simple name used in checks in this app
 * @param host
 * @returns {string|boolean}
 */
let getSearchEnginge = function(host)
{
    // Declare the supported search engines
    let engines = [
        "google",
        "bing",
        "duckduckgo",
        "yahoo",
        "dogpile",
        "ecosia"
    ];

    for(let i = 0; i < engines.length; i++)
    {
        // If the hostname contains one of the defined engines
        // return the defined name
        if (host.indexOf(engines[i]) !== -1)
        {
            return engines[i];
        }
    }

    return false;
};

/**
 * Generate the standard description based on the url (basically concatenation)
 * @param url
 * @returns {string}
 */
let generateGeneralDescription = function(url)
{
    return "Visited webpage at URL: {0}".format(url);

};

/**
 * If the url is connected to a search engine we generate a more specific description
 * If the search query is defined we generate a description specifying what we searched for
 * on which engine. Otherwise we just specify that the given search engine was used
 * @param search_query
 * @param engine
 * @returns {string}
 */
let generateSearchDescription = function(search_query, engine)
{
    // Uppercase the first letter of the given engine
    let engine_name = engine[0].toUpperCase() + engine.slice(1);

    if (search_query !== undefined)
    {
        // Normalize the given search query so we're able to read it properly
        let decoded_search_query = normalizeGetParameter(search_query);

        return "Searched using engine: {0} using term [{1}]".format(engine_name, decoded_search_query);
    }

    return "Visited search engine: {0}".format(engine_name);
};

/**
 * Generate a description based on the given URL
 * @param url
 * @returns {string}
 */
let generateLogDescription = function(url)
{
    let $parsed_url = parseUrl(url);

    // Obtain the hostname of the given URL
    let hostname = $parsed_url.prop('hostname');

    // Check if the URL belongs to a supported search engine
    let parsed_hostname = getSearchEnginge(hostname);

    // If the URL isn't a search engine we generate and return the general description
    if (parsed_hostname === false)
    {
        return generateGeneralDescription(url);
    }
    else {
        // Define a holder for the search query
        let search_query = "";

        // Define a parse strategy for each of the supported search engines
        switch (parsed_hostname) {
            case "google":
            case "bing":
            case "duckduckgo":
            case "ecosia":
            case "dogpile":

                search_query = getUrlParameter(url, 'q');
                return generateSearchDescription(search_query, parsed_hostname);

            case "yahoo":

                search_query = getUrlParameter(url, 'p');
                return generateSearchDescription(search_query, parsed_hostname);
        }
    }
};