chrome.runtime.onMessage.addListener(function(request, sender) {

    // Based on the requested action from the content script we decide what action to take
    switch(request.action)
    {
        case "screenshot_permission":
            // Check in the local storage if the plugin is activated
            // If so we send back true if not, we don't need to send anything
            // as we don't expect any action by the content script
            chrome.storage.local.get([
                "activated"
            ], function(response) {

                chrome.tabs.sendMessage(sender.tab.id, {action: request.action, data: response.activated});

            });
            break;

        case "screenshot_content":
            // Here we send the screenshot created by the content script
            // to Cajen along with URL data and time

            let datetime = new Date();
            let datetime_string = datetime.getTime().toString().slice(0, 10);

            // Sanitize the uri to prevent slashes from ending up in the file name
            let file_save_uri = encodeURI(sender.url).replace(/\//g, '-');
            // Generate a filename for the screenshot consisting of the timestamp
            // and sanitized url, we also truncate the name at 100 characters
            let file_name = "screenshot-{0}-{1}".format(datetime_string, file_save_uri).slice(0, 100);
            let file_name_with_extension = "{0}.jpg".format(file_name);

            chrome.storage.local.get([
                "case_id",
                "log_id",
                "bearer",
                "host",
                "port"
            ], function(result) {

                let cajenApi = new CajenApi(
                    result.bearer,
                    result.host,
                    result.port
                );

                // First we upload the base64 string, making it a file in the active Cajen case
                // On the response we receive the newly created file id. We can use that to create
                // the log entry.
                cajenApi.uploadB64File(result.case_id, file_name_with_extension, request.data, function(resp) {

                    console.log(resp);

                    let description = generateLogDescription(sender.url);

                    cajenApi.saveEntry(
                        result.case_id,
                        result.log_id,
                        description,
                        datetime_string,
                        resp.file_id,
                        function(resp) {
                            console.log(resp);
                        }
                    )

                });

            });

            break;
    }

});